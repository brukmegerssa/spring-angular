## Getting Started

# Installing Angular

1. Install [Node.js](https://nodejs.org/en/) and npm.
2. Install the Angular 10 CLI globally using npm:
   `npm i -g @angular/cli@10.0.6`

# Cloning
1. Open Terminal and goto path/to/your-working-dir
2. Run command `git clone https://gitlab.com/brukmegerssa/spring-angular.git`

# Mysql server setup
1. Mysql server can be downloaded from [official website](https://dev.mysql.com/downloads/mysql/) 
2. Set **username** `root` and **password** `""` 
3. If you already have a MySQL server installed with your own credentials, configure on  path/to/your-working-dir/spring-angular/api/src/main/resources/application.properties

# Run Spring Boot project
1. Make sure you have JDK v1.8 or higher on your machine. If not installed, the required version can be downloaded from [official website](https://www.oracle.com/java/technologies/downloads/)
2. Open api folder on IntelliJ IDEA
3. Press green right arrow button to start the api server 

# Running angular project

1. Open Terminal and goto path/to/your-working-dir/spring-angular
2. Run command `cd ./sms`
3. Run command `npm install`
4. Run command `ng serve`
5. Open `http://localhost:4200` on your browser.
6. Use **Login credential:**
    **Email:** admin@mail.com
    **Password:** Pass@123 

## Demo
![demo](https://gitlab.com/brukmegerssa/spring-angular/-/raw/d58a14c37c669a4ec57738866bc1275a23b62297/demo.gif)