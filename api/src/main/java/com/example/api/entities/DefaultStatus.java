package com.example.api.entities;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class DefaultStatus {
    private Boolean success;
}
