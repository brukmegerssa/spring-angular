package com.example.api.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "students")
@Builder
public class StudentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false)
    private String firstName;
    @Column(nullable = false)
    private String lastName;
    @Column(nullable = false)
    private Instant dob;
    @Column(nullable = false)
    private String gender;
    @Column(nullable = false)
    private String section;
    @Lob
    @Column(name = "photo",length = 2000000)
    private byte[] photo;
    @Column(nullable = false)
    private double totalMark;
    @Column(nullable = false)
    private String address;
    private Instant createdAt = Instant.now();
    private Instant updatedAt = Instant.now();

    public StudentEntity toDto() {
        StudentEntity studentDto = new StudentEntity();
        studentDto.setFirstName(this.firstName);
        studentDto.setLastName(this.lastName);
        studentDto.setDob(this.dob);
        studentDto.setGender(this.gender);
        studentDto.setSection(this.section);
        studentDto.setTotalMark(this.totalMark);
        studentDto.setAddress(this.address);
        return studentDto;
    }
}
