package com.example.api.repositories;

import com.example.api.entities.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity, Integer> {
    @Query("SELECT s FROM StudentEntity s WHERE CONCAT(s.firstName, ' ', s.lastName) LIKE CONCAT(:q,'%')")
    List<StudentEntity> search(@Param("q") String q);
}
