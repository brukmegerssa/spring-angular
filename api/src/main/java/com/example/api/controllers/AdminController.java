package com.example.api.controllers;

import com.example.api.entities.AdminEntity;
import com.example.api.entities.Token;
import com.example.api.services.JwtService;
import com.example.api.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/")
public class AdminController {
    @Autowired
    private JwtService jwtService;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private StudentService service;

    @PostMapping("/authenticate")
    public ResponseEntity<Token> authenticateAndGetToken(@RequestBody AdminEntity admin) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(admin.getEmail(), admin.getPassword()));
        if (authentication.isAuthenticated()) {
            return ResponseEntity.ok(new Token(jwtService.generateToken(admin.getEmail())));
        } else {
            throw new UsernameNotFoundException("invalid user request !");
        }
    }

    @GetMapping("/{id}/photo")
    public ResponseEntity<?> download(@PathVariable int id){
        byte[] imageData = service.download(id);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(imageData);
    }
}
