package com.example.api.controllers;

import com.example.api.entities.DefaultStatus;
import com.example.api.entities.StudentEntity;
import com.example.api.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/student/")
@CrossOrigin
public class StudentController {
    private StudentService service;

    @Autowired
    public StudentController(StudentService service) {
        this.service = service;
    }

    @PostMapping("/create")
    public ResponseEntity<StudentEntity> create(@RequestBody StudentEntity student){
        return  ResponseEntity.ok(service.create(student.toDto()));
    }

    @GetMapping("/all")
    public Object getStudents() {
        return  ResponseEntity.ok(service.getStudents());
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentEntity> getStudent(@PathVariable int id) {
        StudentEntity student = service.getStudent(id);
        return ResponseEntity.ok(student);
    }

    @GetMapping("/search")
    public ResponseEntity<List<StudentEntity>> searchStudent(@RequestParam("q") String q) {
        List <StudentEntity> user= service.searchStudent(q);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/{id}/delete")
    public  ResponseEntity<DefaultStatus> delete(@PathVariable int id){
        return ResponseEntity.ok(service.delete(id));
    }

    @PutMapping("/{id}/update")
    public  ResponseEntity<StudentEntity> update(@RequestBody StudentEntity student,@PathVariable int id){
        return ResponseEntity.ok(service.update(student.toDto(), id));
    }

    @PostMapping("/{id}/upload")
    public ResponseEntity<StudentEntity> upload(@RequestParam("photo")MultipartFile file, @PathVariable int id) throws IOException {
        return ResponseEntity.ok(service.upload(file, id));
    }

    @GetMapping("/{id}/photo")
    public ResponseEntity<?> download(@PathVariable int id){
        byte[] imageData = service.download(id);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.valueOf("image/png"))
                .body(imageData);
    }
}
