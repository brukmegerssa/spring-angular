package com.example.api.services;

import java.io.IOException;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.api.entities.DefaultStatus;
import com.example.api.entities.StudentEntity;
import com.example.api.exceptions.NotFoundException;
import com.example.api.repositories.StudentRepository;
import com.example.api.utils.ImageUtils;

@Service
public class StudentService {
    StudentRepository repository;

    @Autowired
    public StudentService(StudentRepository repository) {
        this.repository = repository;
    }

    public StudentEntity create(StudentEntity student) {
        return repository.save(student);
    }

    public List<StudentEntity> getStudents() {
        return repository.findAll();
    }

    public StudentEntity getStudent(int id) {
        return repository.findById(id).orElseThrow(NotFoundException::new);
    }

    public StudentEntity update(StudentEntity studentData, int id) {
        StudentEntity student = repository.findById(id).orElseThrow(NotFoundException::new);
        student.setFirstName(studentData.getFirstName());
        student.setLastName(studentData.getLastName());
        student.setDob(studentData.getDob());
        student.setGender(studentData.getGender());
        student.setSection(studentData.getSection());
        student.setTotalMark(studentData.getTotalMark());
        student.setAddress(studentData.getAddress());
        student.setUpdatedAt(Instant.now());

        return repository.save(student);
    }

    public StudentEntity upload(MultipartFile file, int id) throws IOException {
        StudentEntity student = repository.findById(id).orElseThrow(NotFoundException::new);
        student.setPhoto(ImageUtils.compressImage(file.getBytes()));
        student.setUpdatedAt(Instant.now());
        return repository.save(student);
    }

    public byte[] download(int id){
        Optional<StudentEntity> dbImageData = repository.findById(id);
        byte[] images=ImageUtils.decompressImage(dbImageData.get().getPhoto());
        return images;
    }

    public DefaultStatus delete(int id)
    {
        repository.findById(id).orElseThrow(NotFoundException::new);
        repository.deleteById(id);
        return new DefaultStatus(true);
    }

    public List<StudentEntity> searchStudent(String q)
    {
        return repository.search(q);
    }
}
