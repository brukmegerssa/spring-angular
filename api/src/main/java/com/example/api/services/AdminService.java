package com.example.api.services;

import com.example.api.config.AdminDetails;
import com.example.api.entities.AdminEntity;
import com.example.api.repositories.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AdminService implements UserDetailsService {
    @Autowired
    private AdminRepository repository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<AdminEntity> adminInfo = repository.findByEmail(email);
        return adminInfo.map(AdminDetails::new)
                .orElseThrow(() -> new UsernameNotFoundException("user not found " + email));
    }
}
