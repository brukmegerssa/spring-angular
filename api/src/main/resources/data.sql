CREATE DATABASE IF NOT EXISTS sms;

CREATE TABLE IF NOT EXISTS  `admins` (
  `id` int NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `admins` (`id`, `email`, `full_name`, `password`) VALUES
(1, 'admin@mail.com', 'Admin', '$2a$10$T.fXTRcVSo1Z5MqsHYLz0utAIHGPVWFo.oHZPdplbdZjshWRPcEJG');