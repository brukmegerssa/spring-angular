import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CredentialModel } from '../../models/credential.model';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  form: FormGroup;
  loading: boolean = false;
  errorMsg: string;

  constructor(private fb: FormBuilder, private service: AuthService) { 
    this.form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
  }

  onSubmit(){
    if (this.form.valid) {
      this.loading = true;
      this.errorMsg = null;
      const credential: CredentialModel = {
        email: this.form.get('email').value,
        password: this.form.get('password').value,
      };
      this.service
        .authenticate(credential)
        .pipe(
          catchError((error) => {
            this.loading = false;
            this.errorMsg = "Invalid Email/Password";
            return of(null);
          })
        )
        .subscribe(() => {
          this.loading = false;
        });
    }
  }

}
