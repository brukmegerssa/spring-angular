import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/shared/services/base.service';

import { map } from 'rxjs/operators';

import {JwtHelperService} from '@auth0/angular-jwt';

import * as moment from 'moment';
import { Router } from '@angular/router';
import { CredentialModel } from '../models/credential.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {
  private ACCESS_TOKEN = 'auth_token';
  private EXPIRES_AT = 'expires_at';
  constructor(private http: HttpClient, private router: Router) {
    super('admin');
  }

  authenticate(credential: CredentialModel) {
    return this.http.post<{token: string}>(this.url('authenticate'), credential).pipe(
      map((res: any) => {
        if (res && res.token) {
          this.storeToken(res);
        }
        return res;
      })
    );
  }

  public decodeToken(token) {
    return new JwtHelperService().decodeToken(token);
  }

  private storeToken(res: { token: string }) {
    const decodedToken = this.decodeToken(res.token);
    const expiresAt = moment.unix(decodedToken.exp);

    localStorage.setItem(this.ACCESS_TOKEN, res.token);
    localStorage.setItem(this.EXPIRES_AT, JSON.stringify(expiresAt.valueOf()));

    this.router.navigateByUrl(`/dashboard`);
  }

  public getToken() {
    return localStorage.getItem(this.ACCESS_TOKEN);
  }

  isLoggedIn() {
    const token = this.getToken();

    if (!token) {
      return false;
    }

    const jwtHelper = new JwtHelperService();
    const isExpired = jwtHelper.isTokenExpired(token);
    return !isExpired;
  }

  logOut() {
    localStorage.removeItem(this.ACCESS_TOKEN);
    this.router.navigateByUrl(``);
  }

  public getCurrentUser() {
    return this.decodeToken(this.getToken());
  }
}
