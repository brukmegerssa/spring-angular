import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthComponent } from './auth.component';
import { AuthGuardService } from './services/auth-guard.service';

const routes: Routes = [{ path: '', component: AuthComponent, canActivate: [AuthGuardService], }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }
