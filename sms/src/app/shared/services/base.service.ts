import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  baseUrl: string = environment.apiAddress;

  constructor(private prefix: string) {}

  url(suffix: string) {
    return `${this.baseUrl}${this.prefix}/${suffix}`;
  }
}
