import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { environment } from 'src/environments/environment';
import { StudentForm } from '../../models/student-form.model';
import { Student } from '../../models/student.model';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.scss'],
})
export class AddFormComponent implements OnInit {
  form: FormGroup;
  loading: boolean = false;
  imgSrc: string;

  uploading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private service: DashboardService,
    public dialogRef: MatDialogRef<AddFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Student
  ) {
    this.form = this.fb.group({
      address: ['', Validators.required],
      dob: [null, Validators.required],
      firstName: ['', Validators.required],
      gender: [null, Validators.required],
      lastName: ['', Validators.required],
      section: ['', Validators.required],
      totalMark: [null, Validators.required],
    });
  }

  get maxDate() {
    return new Date();
  }

  ngOnInit(): void {
    if (this.data) {
      this.imgSrc = this.data.photo ? `${environment.apiAddress}admin/${this.data.id}/photo` : null;
      this.form.patchValue({
        address: this.data.address,
        dob: this.data.dob,
        firstName: this.data.firstName,
        gender: this.data.gender,
        lastName: this.data.lastName,
        section: this.data.section,
        totalMark: this.data.totalMark,
      });
    }
  }

  uploadFile(event: Event) {
    const element = event.currentTarget as HTMLInputElement;
    let fileList: FileList = element.files;
    if (fileList) {
      this.uploading = true;
      this.imgSrc = URL.createObjectURL(fileList[0]);
      this.service.upload(fileList[0], this.data.id).subscribe((res) => {
        if (res) {
          this.dialogRef.close({ data: res, new: false });
          this.uploading = false;
        }
      });
    }
  }

  onSubmit() {
    const data: StudentForm = {
      firstName: this.form.get('firstName')!.value,
      lastName: this.form.get('lastName')!.value,
      dob: this.form.get('dob')!.value,
      gender: this.form.get('gender')!.value,
      address: this.form.get('address')!.value,
      section: this.form.get('section')!.value,
      totalMark: this.form.get('totalMark')!.value,
    };
    const updateData: Student = {
      ...this.data,
      ...data,
    };

    if (this.form.valid) {
      if (this.data) {
        this.service.update(updateData).subscribe((res) => {
          if (res) {
            this.dialogRef.close({ data: res, new: false });
          }
        });
      } else {
        this.service.create(data).subscribe((res) => {
          if (res) {
            this.dialogRef.close({ data: res, new: true });
          }
        });
      }
    }
  }
}
