import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Student } from '../../models/student.model';
import { DashboardService } from '../../services/dashboard.service';
import { AddFormComponent } from '../add-form/add-form.component';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  form: FormGroup;
  results: Student[] = [];

  constructor(private fb: FormBuilder, private service: DashboardService, public dialog: MatDialog) { 
    this.form = this.fb.group({
      q: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.form.valueChanges.subscribe(q => {
      if (this.form.valid) {
        this.service.search(q.q).subscribe((res) => {
          this.results = res;
        });
      } else {
        this.results = [];
      }
    })
  }

  detail(data?: Student) {
    const dialogRef = this.dialog.open(AddFormComponent, {
      width: '40%',
      data: data,
    });
    
    this.form.reset()
  }
}
