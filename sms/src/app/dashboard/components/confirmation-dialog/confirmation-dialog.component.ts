import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Student } from '../../models/student.model';
import { DashboardService } from '../../services/dashboard.service';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.scss'],
})
export class ConfirmationDialogComponent implements OnInit {
  constructor(
    private service: DashboardService,
    public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Student
  ) {}

  ngOnInit(): void {}

  deleteStudent() {
    if (this.data) {
      this.service.delete(this.data.id).subscribe(() => {
        this.dialogRef.close(true);
      });
    }
  }
}
