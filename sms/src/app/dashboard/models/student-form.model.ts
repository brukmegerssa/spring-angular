export interface StudentForm{
    firstName: string;
    lastName: string;
    dob: string;
    gender: string;
    address: string;
    section: string;
    totalMark: number;
}