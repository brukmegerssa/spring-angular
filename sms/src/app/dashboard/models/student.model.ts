export interface Student{
    id: number;
    firstName: string;
    lastName: string;
    dob: string;
    gender: string;
    address: string;
    section: string;
    photo?: any;
    totalMark: number;
    createdAt: string;
    updatedAt: string;
}