import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from '../auth/services/auth.service';
import { AddFormComponent } from './components/add-form/add-form.component';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { Student } from './models/student.model';
import { DashboardService } from './services/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  displayedColumns: string[] = [
    'fistName',
    'LastName',
    'dob',
    'gender',
    'address',
    'section',
    'totalMark',
    'createdAt',
    'lastModified',
    'actions',
  ];
  students: Student[] = [];

  constructor(
    private service: DashboardService,
    public dialog: MatDialog,
    public authService: AuthService
  ) {}

  ngOnInit(): void {
    this.service.getStudents().subscribe((res) => {
      this.students = res;
    });
  }

  openDialog(data?: Student) {
    const dialogRef = this.dialog.open(AddFormComponent, {
      width: '40%',
      data: data,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result.new) {
        this.students = [result.data, ...this.students];
      } else if (result.new === false) {
        this.students = this.students.map((s) => {
          if (s.id === data.id) {
            return result.data;
          }
          return s;
        });
      }
    });
  }
  deleteStudent(data: Student) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: data,
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result) {
        this.students = this.students.filter((s) => s.id != data.id);
      }
    });
  }
}
