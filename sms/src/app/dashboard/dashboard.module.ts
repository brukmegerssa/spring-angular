import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { AddFormComponent } from './components/add-form/add-form.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { TimeagoModule } from 'ngx-timeago';
import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { AgePipe } from './pipes/age.pipe';

@NgModule({
  declarations: [
    DashboardComponent,
    AddFormComponent,
    ConfirmationDialogComponent,
    SearchFormComponent,
    AgePipe,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    TimeagoModule,
  ],
})
export class DashboardModule {}
