import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/shared/services/base.service';
import { StudentForm } from '../models/student-form.model';
import { Student } from '../models/student.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardService extends BaseService {
  constructor(private http: HttpClient) {
    super('student');
  }

  getStudents() {
    return this.http.get<Student[]>(this.url('all'));
  }

  create(data: StudentForm) {
    return this.http.post<Student>(this.url('create'), data);
  }

  update(data: Student) {
    return this.http.put<Student>(this.url(`${data.id}/update`), data);
  }

  delete(id: number) {
    return this.http.delete(this.url(`${id}/delete`));
  }

  upload(file: File, id: number){
    const formData = new FormData();
    formData.append('photo', file);
    return this.http.post<Student>(this.url(`${id}/upload`), formData);
  }
  
  search(q: string) {
    return this.http.get<Student[]>(this.url('search'), {
      params: {q},
    });
  }
}
